sap.ui.define([
	// "sap/ui/core/mvc/Controller",
	"sap/ui/base/Object",
	"sap/m/MessageBox",
	// "sap/m/IconTabBar",
	"sap/ui/model/json/JSONModel",
], function (Object, momentjs, BaseUtility, MessageBox, ValidatorForm, JSONModel) {
	"use strict";
	return Object.extend("com.gc.scorecards.SampleScoreCards.js.libs.BaseUtility", {
		constructor: function () {

		},
		createTabIcon: function (_oId, _oActionEvent, _oStyleClass) {
			var oIconTabBar = new sap.m.IconTabBar(_oId, {
				expandable: false,
				expanded: true,
				select: _oActionEvent
			});
			oIconTabBar.addStyleClass("sapUiResponsiveContentPadding " + _oStyleClass);

			return oIconTabBar;
		},
		// (oContext.getObject().DataName + "-P", "panelTile", oContext.getObject().DataPrettyName, oHBox);
		createPanel: function (_oId, _oStyleClass, _oText, _oContent) {

			var oPanel = new sap.m.Panel("panel" + _oId, {
				backgroundDesign: "Transparent",
				headerText: _oText,
				content: _oContent
			});
			oPanel.addStyleClass(_oStyleClass);

			return oPanel;
		},
		createIconTabFilter: function (_oId, _oIcon, _oText, _oContent) {

			var oIconTabFilter = new sap.m.IconTabFilter("tab-" + _oId, {
				icon: _oIcon,
				text: _oText,
				key: "iconTab-" + _oId,
				content: _oContent

			})

			return oIconTabFilter;
		},

		createHBox: function (_oId, _oStyleClass) {
			var oHBox = new sap.m.HBox(_oId, {

			});
			oHBox.addStyleClass(_oStyleClass);
			return oHBox;
		},

		createVBox: function (_oId, _oDirction, _oSryleClass) {
			var oVBox = new sap.m.VBox(_oId, {
				direction: _oDirction
			});
			oVBox.addStyleClass(_oSryleClass);

			return oVBox
		},

		createCustomTileScoreCard: function (_oContent, _oSryleClass) {
			var oTile = new sap.m.CustomTile({
				content: _oContent
			});
			oTile.addStyleClass(_oSryleClass);
			return oTile;
		},

		createLabel: function (_oId, _oText) {
			var oLabel = new sap.m.Label(_oId, {
				width: "100%",
				wrapping: true,
				displayOnly:true,
				textAlign: "Right",
				vAlign: "Middle",
				text: _oText
			});
			return oLabel;
		}

	});

});