sap.ui.define([
	// "sap/ui/core/mvc/Controller",
	"sap/ui/base/Object",
	"sap/m/MessageBox",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
], function (Object, momentjs, BaseUtility, MessageBox, ValidatorForm, JSONModel, Filter, FilterOperator) {
	"use strict";
	return Object.extend("com.gc.scorecards.SampleScoreCards.js.libs.OdataUtility", {
		constructor: function () {

		},
		getModelDetailScores: function (_ofilter, _oThis) {
			var oController = _oThis;
			var ofilter = _ofilter;
			// var filters = [new Filter("Month", sap.ui.model.FilterOperator.EQ, "01")];
			return new Promise(function (resolve, reject) {

				oController.getView().getModel().read("/ScoreCardsSet", {
					filters: ofilter,
					success: jQuery.proxy(function (response) {
						resolve(response)
							// console.log(response)
					}, oController),
					error: jQuery.proxy(function (response) {
						reject(response);
					}, oController)
				});

			}.bind(this));
		},

		getModelConfigGauge: function (oBestRef, oWorstRef, oColorRef, oMaxErrorRange, oMaxWarningRange, oDividers, oReverseColors, oDecimal) {
			var colorError = "#F03E3E";
			var colorSuccess = "#30B32D";
			if (oReverseColors) {
				colorError = "#30B32D";
				colorSuccess = "#F03E3E";
			}
			var configGauge = {
				angle: -0.2,
				lineWidth: 0.2,
				radiusScale: 1,
				pointer: {
					length: 0.49,
					strokeWidth: 0.02,
					color: "#ffffff"
				},
				limitMax: false,
				limitMin: false,
				strokeColor: "#ffffff",
				generateGradient: true,
				highDpiSupport: true,
				percentColors: [
					[0, "#a9d70b"],
					[0.5, "#f9c802"],
					[1, "ff0000"]
				],
				renderTicks: {
					divisions: oDividers,
					divWidth: 0.3,
					divLength: 0.67,
					divColor: "#fff",
					subDivisions: 0,
					subLength: 0.3,
					subWidth: 0.1,
					subColor: "#fff"
				},
				staticLabels: {
					font: "10px sans-serif",
					labels: [oWorstRef, oBestRef],
					color: "#fff",
					fractionDigits: oDecimal
				},
				staticZones: [{
					strokeStyle: colorError,
					min: oWorstRef,
					max: oMaxErrorRange
				}, {
					strokeStyle: "#FFDD00",
					min: oMaxErrorRange,
					max: oMaxWarningRange
				}, {
					strokeStyle: colorSuccess,
					min: oMaxWarningRange,
					max: oBestRef
				}]
			};

			return configGauge;
		},

		getModelDetailScoresMock: function (_oThis) {
			var oController = _oThis
			return new Promise(function (resolve, reject) {
				var json = {
					"results": [{

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "puntos",
							"DataName": "total_points",
							"DataPrettyName": "Puntos",
							"DataType": "main",
							"DataValue": "90.4123",
							"DataParentName": "",
							"DataParentType": "",
							"WorstRef": "",
							"TargetRef": "",
							"BestRef": "",
							"DataBackgroundColor": "#b1b3b3",
							"DataPosition": "00010"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "",
							"DataName": "rating",
							"DataPrettyName": "Clasificación",
							"DataType": "main",
							"DataValue": "262",
							"DataParentName": "",
							"DataParentType": "",
							"WorstRef": "331.0",
							"TargetRef": "",
							"BestRef": "1.0",
							"DataBackgroundColor": "#b1b3b3",
							"DataPosition": "00020"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "",
							"DataName": "equipment_type",
							"DataPrettyName": "Equipo",
							"DataType": "main",
							"DataValue": "Camion",
							"DataParentName": "",
							"DataParentType": "",
							"WorstRef": "",
							"TargetRef": "",
							"BestRef": "",
							"DataBackgroundColor": "#b1b3b3",
							"DataPosition": "00030"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "",
							"DataName": "property",
							"DataPrettyName": "Properidad",
							"DataType": "tab",
							"DataValue": "",
							"DataParentName": "",
							"DataParentType": "",
							"WorstRef": "",
							"TargetRef": "",
							"BestRef": "",
							"DataBackgroundColor": "#eaaa00",
							"DataPosition": "00040"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "",
							"DataName": "people",
							"DataPrettyName": "Personas",
							"DataType": "tab",
							"DataValue": "",
							"DataParentName": "",
							"DataParentType": "",
							"WorstRef": "",
							"TargetRef": "",
							"BestRef": "",
							"DataBackgroundColor": "#b1b3b3",
							"DataPosition": "00040"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "",
							"DataName": "production",
							"DataPrettyName": "Produccion",
							"DataType": "tab",
							"DataValue": "",
							"DataParentName": "",
							"DataParentType": "",
							"WorstRef": "",
							"TargetRef": "",
							"BestRef": "",
							"DataBackgroundColor": "#d9c89e",
							"DataPosition": "00040"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "puntos",
							"DataName": "avg_crew_points",
							"DataPrettyName": "Puntos meta/equipo",
							"DataType": "main",
							"DataValue": "103.357",
							"DataParentName": "",
							"DataParentType": "",
							"WorstRef": "",
							"TargetRef": "",
							"BestRef": "",
							"DataBackgroundColor": "#b1b3b3",
							"DataPosition": "00040"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "puntos",
							"DataName": "safety_points",
							"DataPrettyName": "Seguridad",
							"DataType": "points",
							"DataValue": "20",
							"DataParentName": "total_points",
							"DataParentType": "main",
							"WorstRef": "0.0",
							"TargetRef": "10.0",
							"BestRef": "20.0",
							"DataBackgroundColor": "#eaaa00",
							"DataPosition": "00050"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "puntos",
							"DataName": "absenteeism_points",
							"DataPrettyName": "Ausentismo",
							"DataType": "points",
							"DataValue": "8",
							"DataParentName": "total_points",
							"DataParentType": "main",
							"WorstRef": "0.0",
							"TargetRef": "10.0",
							"BestRef": "20.0",
							"DataBackgroundColor": "#eaaa00",
							"DataPosition": "00060"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "puntos",
							"DataName": "dem_points",
							"DataPrettyName": "DEM",
							"DataType": "points",
							"DataValue": "13",
							"DataParentName": "total_points",
							"DataParentType": "main",
							"WorstRef": "0.0",
							"TargetRef": "10.0",
							"BestRef": "20.0",
							"DataBackgroundColor": "#89d4eb",
							"DataPosition": "00070"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "puntos",
							"DataName": "spot_points",
							"DataPrettyName": "Aculatam.",
							"DataType": "points",
							"DataValue": "10.3363",
							"DataParentName": "total_points",
							"DataParentType": "main",
							"WorstRef": "0.0",
							"TargetRef": "20.0",
							"BestRef": "30.0",
							"DataBackgroundColor": "#d9c89e",
							"DataPosition": "00080"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "puntos",
							"DataName": "speed_points",
							"DataPrettyName": "Velocidad",
							"DataType": "points",
							"DataValue": "18.4576",
							"DataParentName": "total_points",
							"DataParentType": "main",
							"WorstRef": "0.0",
							"TargetRef": "20.0",
							"BestRef": "30.0",
							"DataBackgroundColor": "#d9c89e",
							"DataPosition": "00100"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "puntos",
							"DataName": "dump_points",
							"DataPrettyName": "Vaciado",
							"DataType": "points",
							"DataValue": "10.1726",
							"DataParentName": "total_points",
							"DataParentType": "main",
							"WorstRef": "0.0",
							"TargetRef": "10.0",
							"BestRef": "20.0",
							"DataBackgroundColor": "#d9c89e",
							"DataPosition": "00110"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "puntos",
							"DataName": "delay_points",
							"DataPrettyName": "Demoras",
							"DataType": "points",
							"DataValue": "4.65475",
							"DataParentName": "total_points",
							"DataParentType": "main",
							"WorstRef": "0.0",
							"TargetRef": "15.0",
							"BestRef": "30.0",
							"DataBackgroundColor": "#d9c89e",
							"DataPosition": "00130"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "puntos",
							"DataName": "data_quality_points",
							"DataPrettyName": "Calidad de info",
							"DataType": "points",
							"DataValue": "5.79114",
							"DataParentName": "total_points",
							"DataParentType": "main",
							"WorstRef": "0.0",
							"TargetRef": "5.0",
							"BestRef": "10.0",
							"DataBackgroundColor": "#89d4eb",
							"DataPosition": "00140"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "cantidad",
							"DataName": "loads_shiftload_count HA",
							"DataPrettyName": "Cargas HA",
							"DataType": "kpi",
							"DataValue": "177",
							"DataParentName": "production",
							"DataParentType": "tab",
							"WorstRef": "",
							"TargetRef": "",
							"BestRef": "",
							"DataBackgroundColor": "#b1b3b3",
							"DataPosition": "00170"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "cantidad",
							"DataName": "SAFETYssss HA",
							"DataPrettyName": "Seguridad HA",
							"DataType": "kpi",
							"DataValue": "0",
							"DataParentName": "people",
							"DataParentType": "tab",
							"WorstRef": "",
							"TargetRef": "",
							"BestRef": "",
							"DataBackgroundColor": "#eaaa00",
							"DataPosition": "00180"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "cantidad",
							"DataName": "FAISSS HA",
							"DataPrettyName": "FAI HA",
							"DataType": "kpi",
							"DataValue": "0",
							"DataParentName": "people",
							"DataParentType": "tab",
							"WorstRef": "",
							"TargetRef": "",
							"BestRef": "",
							"DataBackgroundColor": "#eaaa00",
							"DataPosition": "00180"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "cantidad",
							"DataName": "absentee HA",
							"DataPrettyName": "Ausentismo HA",
							"DataType": "kpi",
							"DataValue": "1",
							"DataParentName": "people",
							"DataParentType": "tab",
							"WorstRef": "",
							"TargetRef": "",
							"BestRef": "",
							"DataBackgroundColor": "#eaaa00",
							"DataPosition": "00220"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "cantidad",
							"DataName": "DEMSSS HA",
							"DataPrettyName": "DEM HA",
							"DataType": "kpi",
							"DataValue": "0",
							"DataParentName": "property",
							"DataParentType": "tab",
							"WorstRef": "",
							"TargetRef": "",
							"BestRef": "",
							"DataBackgroundColor": "#89d4eb",
							"DataPosition": "00230"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "cantidad",
							"DataName": "DEMOPsss HA",
							"DataPrettyName": "DEMOP HA",
							"DataType": "kpi",
							"DataValue": "2",
							"DataParentName": "property",
							"DataParentType": "tab",
							"WorstRef": "",
							"TargetRef": "",
							"BestRef": "",
							"DataBackgroundColor": "#89d4eb",
							"DataPosition": "00240"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "seg",
							"DataName": "raw_spotting_time HA",
							"DataPrettyName": "Aculatam. (s) HA",
							"DataType": "kpi",
							"DataValue": "73.0264",
							"DataParentName": "production",
							"DataParentType": "tab",
							"WorstRef": "81.0",
							"TargetRef": "66.0",
							"BestRef": "51.0",
							"DataBackgroundColor": "#d9c89e",
							"DataPosition": "00250"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "km/h",
							"DataName": "raw_speed HA",
							"DataPrettyName": "Velocidad (km/h) HA",
							"DataType": "kpi",
							"DataValue": "21.2957",
							"DataParentName": "production",
							"DataParentType": "tab",
							"WorstRef": "19.5",
							"TargetRef": "21.5",
							"BestRef": "22.5",
							"DataBackgroundColor": "#d9c89e",
							"DataPosition": "00290"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "seg",
							"DataName": "raw_dumptime HA",
							"DataPrettyName": "Vaciado(s) HA",
							"DataType": "kpi",
							"DataValue": "92.3116",
							"DataParentName": "production",
							"DataParentType": "tab",
							"WorstRef": "110.0",
							"TargetRef": "90.0",
							"BestRef": "80.0",
							"DataBackgroundColor": "#d9c89e",
							"DataPosition": "00300"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "min",
							"DataName": "raw_delay_time HA",
							"DataPrettyName": "Demoras (min/turno) HA",
							"DataType": "kpi",
							"DataValue": "62.8968",
							"DataParentName": "production",
							"DataParentType": "tab",
							"WorstRef": "66.0",
							"TargetRef": "56.0",
							"BestRef": "46.0",
							"DataBackgroundColor": "#d9c89e",
							"DataPosition": "00310"
						},
						"Passportid": "10702388"
					}, {

						"Header_Score": {

							"Name": "Lizbeth Zuñiga"
						},
						"Item_Score": {
							"DataUnits": "%",
							"DataName": "raw_data_quality HA",
							"DataPrettyName": "Calidad de info (%) HA",
							"DataType": "kpi",
							"DataValue": "84.2255",
							"DataParentName": "property",
							"DataParentType": "tab",
							"WorstRef": "75.0",
							"TargetRef": "85.0",
							"BestRef": "90.0",
							"DataBackgroundColor": "#89d4eb",
							"DataPosition": "00320"
						},
						"Passportid": "10702388"
					}]

				}

				resolve(json)

			});
		},

	});

});