sap.ui.define([
	// "sap/ui/core/mvc/Controller",
	"sap/ui/base/Object",
	"sap/m/MessageBox",
	// "sap/m/IconTabBar",
	"sap/ui/model/json/JSONModel",
	"com/gc/scorecards/SampleScoreCards/js/libs/OdataUtility",
], function (Object, momentjs, BaseUtility, MessageBox, ValidatorForm, JSONModel, OdataUtility) {
	"use strict";
	return Object.extend("com.gc.scorecards.SampleScoreCards.js.libs.GaugeUtility", {
		constructor: function () {

		},
		createCanvasContentainerGauge: function (_oVBoxL, _oVBoxR, _oPanel, _oNameGauge) {

			var oVBoxL = _oVBoxL;
			var oVBoxR = _oVBoxR;
			var oNameCanvas = _oNameGauge + "Canvas";
			var oPanel = _oPanel;
			var oHTML = new sap.ui.core.HTML();
			var oContent = "<div class=\"containerGauge\">" + "<canvas id=\"" + oNameCanvas + "\" class=\"canvasMider\"></canvas>" +
				"</div>";
			var oHTMLabel = new sap.ui.core.HTML();
			var oContentLabel = "<div id='" + oNameCanvas + "MiderText' class='textMider'></div>" +
				"<div id='" + oNameCanvas + "Label' class='labelMider'></div>";
			var oValuesGauge = {
				nameCanvas: oNameCanvas,
				nameObject: _oNameGauge,
				panel: oPanel
			};

			return new Promise(function (resolve, reject) {

				oHTML.setContent(oContent);
				oVBoxL.addItem(oHTML);
				oHTMLabel.setContent(oContentLabel);
				oVBoxR.addItem(oHTMLabel);

				oVBoxL.onAfterRendering = function (oEvent) {
					resolve(oValuesGauge);
				};
			});

		},

		createGauge: function (_oDataUtility, _oValues) {

			var oNameCanvas = _oValues.nameCanvas;
			var oNameModel = _oValues.nameObject;
			var oDataUtility = _oDataUtility;

			var oSelectPanel = _oValues.panel;
			var oController = this;
			var oManagerModel = oController.getView().getModel("configManager");
			var oScoreModel = oController.getView().getModel("scoreDataModel");

			var reverseColors = false;

			return new Promise(function (resolve, reject) {

				var oModelData = _.filter(oScoreModel.getProperty("/results"), function (value) {
					if (value.Item_Score.DataName.indexOf(oNameModel) !== -1) {
						return true;
					}
				}.bind(oController));

				oModelData = oModelData[0];

				var decimal = 0;
				if (oModelData.Item_Score.DataName === "raw_speed_HA") {
					decimal = 1;
				}

				var oBestRef = parseFloat(oModelData.Item_Score.BestRef);
				var oWorstRef = parseFloat(oModelData.Item_Score.WorstRef);

				if (oBestRef < oWorstRef) {
					oBestRef = parseFloat(oModelData.Item_Score.WorstRef);
					oWorstRef = parseFloat(oModelData.Item_Score.BestRef);
					reverseColors = true;
				}
				// var oTargetRef = parseInt(oModelData.Item_Score.TargetRef);
				var oTargetRef = parseFloat(oModelData.Item_Score.DataValue);
				// oTargetRef = Math.round(oTargetRef);

				// function hexToRgb(hex) {

				var hex = oModelData.Item_Score.DataBackgroundColor;
				var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
				var colorRequest = result ? {
					r: parseInt(result[1], 16),
					g: parseInt(result[2], 16),
					b: parseInt(result[3], 16)
				} : null;
				// }

				// var oColorRef = oModelData.Item_Score.DataBackgroundColor + "5e";
				var oColorRef = "rgba(" + colorRequest.r + "," + colorRequest.g + "," + colorRequest.b + ", 0.37)";

				var oColorRef2 = oModelData.Item_Score.DataBackgroundColor;
				var putDividers = oBestRef - oWorstRef;
				var divideRange = Math.round(putDividers / 3);
				var maxErrorRange = oWorstRef + divideRange;
				var maxWarningRange = maxErrorRange + divideRange;
				var oDataUnits = oModelData.Item_Score.DataUnit;
				var nameClass = "dynamicCSSClas" + oNameModel;
				var oConfigGauge = oDataUtility.getModelConfigGauge(oBestRef, oWorstRef, oColorRef, maxErrorRange, maxWarningRange, putDividers,
					reverseColors, decimal);

				var gauge = new Gauge(document.getElementById(oNameCanvas)).setOptions(oConfigGauge);
				document.styleSheets.item(2).insertRule("." + nameClass + "{ background-color: " + oColorRef +
					";border-bottom: 6px solid " + oColorRef2 + "; }", 0);

				oSelectPanel.addStyleClass(nameClass);

				gauge.maxValue = oBestRef;
				gauge.setMinValue(oWorstRef);
				gauge.animationSpeed = 15;
				gauge.setTextField(document.getElementById(oNameCanvas + "MiderText"));
				$("#" + oNameCanvas + "Label").html(oDataUnits)
				gauge.textField.fractionDigits = decimal;
				gauge.set(oTargetRef);

				oManagerModel.setProperty("/ViewScoreBusy", false);
				resolve();

			});

		},
	});

});